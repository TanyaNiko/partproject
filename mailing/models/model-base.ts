import { NotifiableObject } from "../../core/diagram/notifiable-object";

export abstract class ModelBase extends NotifiableObject {
    abstract toDto(): any;

    private _hasChanges = false;

    hasChanges(): boolean { return this._hasChanges; }

    notifyPropertyChanged(propName: string) {
        this._hasChanges = true;

        super.notifyPropertyChanged(propName);
    }

    applyChanges() {
        this._hasChanges = false;
    }
}
