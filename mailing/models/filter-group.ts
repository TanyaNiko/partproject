import { Random } from '../../core/services/random.service';
import { Filter } from "./filter";
import { FilterGroupDto } from "../services/mailing-service/filter-group-dto";

export class FilterGroup {
    static fromDto(dto: FilterGroupDto, random: Random): FilterGroup {
        const group = new FilterGroup(dto.id);

        if (dto.filters) {
            group.filters.push(...dto.filters.map(x => Filter.fromDto(x, random)));
        }

        return group;
    }
    
    toDto(): FilterGroupDto {
        return {
            id: this.id,
            filters: this.filters.map(x => x.toDto())
        }
    }

    constructor(public readonly id: string) {
    }

    readonly filters: Filter[] = [];
}
