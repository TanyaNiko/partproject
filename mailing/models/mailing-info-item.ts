import { Lazy } from '../../core/utils';
import { MailingInfoItemDto } from '../services/mailing-service/mailing-info-item-dto';
import { MailingMessengerStatistics, MailingStatistics } from '../services/mailing-service/mailing-messenger-statistics';
import { MailingStatus } from '../services/mailing-service/mailing-status';
import { ModelBase } from './model-base';

export class MailingInfoItem extends ModelBase {
    static readonly empty: MailingInfoItem = new MailingInfoItem(0);

    toDto(): MailingInfoItemDto {
        return {
            id: this.id,
            name: this.name,
            status: this.status,
            startAt: this.startAt,
            messengers: this.messengers.map(x => Object.assign({}, x)),
            type: this.type,
        };
    }

    static fromDto(dto: MailingInfoItemDto): MailingInfoItem {

        const model = new MailingInfoItem(dto.id);

        model.name = dto.name;
        model.startAt = dto.startAt;
        model.sendAt = dto.sendAt;
        model.status = dto.status;
        model.type = dto.type;
        model.messengers.push(...(dto.messengers || []).map(x => Object.assign({}, x)));

        return model;
    }

    private readonly _overallStatistics: Lazy<MailingStatistics>;

    constructor(readonly id: number) {
        super();

        this.status = MailingStatus.Inactive;

        this._overallStatistics = new Lazy<MailingStatistics>(() => {
            const result = {
                sent: 0,
                delivered: 0,
                clicked: 0,
            };

            (this.messengers || []).forEach(x => {
                result.clicked += x.clicked;
                result.delivered += x.delivered;
                result.sent += x.sent;
            });

            return result;
        });
    }

    name: string = '';
    startAt?: string;
    sendAt?: string;
    status?: MailingStatus;
    type?: number;
    readonly messengers: MailingMessengerStatistics[] = [];

    get overallStatistics(): MailingStatistics {
        return this._overallStatistics.value;
    }
}
