import { ITagAutocomplete } from '../../core/api/tags.service/tag-template.dto';
import { Random } from '../../core/services/random.service';
import { FilterType } from "../services/mailing-service/filter-type";
import { FilterDto } from "../services/mailing-service/filter-dto";
import { ModelBase } from './model-base';

export class Filter extends ModelBase {
    static fromDto(dto: FilterDto, random: Random) {

        if (!dto.id) {
            dto.id = random.guid();
        }

        const obj = new Filter(dto.id);
        obj._field = dto.field;
        obj._type = dto.type;
        obj._value = dto.value;
        obj._operand = dto.operand;
        obj._tags = dto.tags ? dto.tags : [];

        return obj;
    }

    toDto(): FilterDto {
        return {
            id: this.id,

            type: this.type,
            value: this.value,

            field: this.field,
            operand: this.operand,

            tags: this._tags,
        };
    }

    private _title: string;
    private _type: FilterType;
    private _value: string;
    private _operand: string;
    private _field?: string;
    private _tags: string[];

    private constructor(public readonly id: string) {
        super();
    }

    public get title(): string {
        return this._title || '';
    }

    public set title(value: string) {
        if (this._title != value) {
            this._title = value;
            this.notifyPropertyChanged('title');
        }
    }

    public get type(): FilterType {
        return this._type;
    }

    public set type(value: FilterType) {
        if (this._type != value) {
            this._type = value;
            this.notifyPropertyChanged('type');
        }
    }

    public get value(): string {
        return this._value;
    }

    public set value(value: string) {
        if (this._value != value) {
            this._value = value;
            this.notifyPropertyChanged('value');
        }
    }

    public get operand(): string {
        return this._operand;
    }

    public get tags(): any {
        return <ITagAutocomplete[]>this._tags.map((tag: string) => {
            return <ITagAutocomplete>{
                text: tag,
            };
        });
    }

    public set tags(text: any) {
        this._tags.push(text);
    }

    public deleteTag(tagToDelete: string): void {
        const index = this._tags.findIndex(tag => tag === tagToDelete);

        if (index !== -1) {
            this._tags.splice(index, 1);
        }
    }

    public set operand(value: string) {
        if (this._operand != value) {
            this._operand = value;
            this.notifyPropertyChanged('operand');
        }
    }

    public get field(): string {
        return this._field;
    }

    public set field(value: string) {
        if (this._field != value) {
            this._field = value;
            this.notifyPropertyChanged('field');
        }
    }
}
