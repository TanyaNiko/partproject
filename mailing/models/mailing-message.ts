import { Random } from '../../core/services/random.service';
import { MailingMessageDto, MessageType } from '../services/mailing-service/mailing-message-dto';
import { MailingMessageItem } from './mailing-message-item';
import { ModelBase } from './model-base';
import * as Enumerable from 'linq';

declare const moment;

export class MailingMessage extends ModelBase {
    static sortByDate(items: MailingMessageDto[]): MailingMessageDto[] {
        const sortedItems = items.sort((a, b) => {
            const ad = moment(a.sendAt).format('YYYYMMDDHHmm');
            const bd = moment(b.sendAt).format('YYYYMMDDHHmm');
            return ad - bd;
        });
        return sortedItems;
    }

    static fromDto(dto: MailingMessageDto, mailingId: number, random: Random): MailingMessage {
        if (!dto.id) {
            dto.id = random.guid();
        }

        const obj = new MailingMessage(dto.id, mailingId);
        obj._name = dto.name;
        obj._type = dto.type;
        obj._sendAt = dto.sendAt ? (moment(dto.sendAt)).format("DD-MM-YYYY HH:mm") : '';
        obj.items.push(...(dto.items || []).map(x => MailingMessageItem.fromDto(x, mailingId, random)));

        return obj;
    }

    toDto(): MailingMessageDto {
        return {
            id: this.id,
            name: this.name,
            sendAt: this.sendAt ? (moment(this.sendAt, "DD-MM-YYYY HH:mm")).toISOString() : null,
            items: this.items.map(x => x.toDto()) || [],
            type: this.type,
        };
    }

    private _type: MessageType;
    private _name: string;
    private _sendAt: string; // null if send immediately -otherwise- date/time string

    private constructor(public readonly id, public readonly mailingId: number) {
        super();
    }

    hasChanges(): boolean { return super.hasChanges() || Enumerable.from(this.items).any(x => x.hasChanges()); }

    public get name(): string {
        return this._name;
    }

    public set name(value: string) {
        if (this._name != value) {
            this._name = value;
            this.notifyPropertyChanged('name');
        }
    }

    public get type(): MessageType {
        return this._type;
    }

    public set type(value: MessageType) {
        if (this._type != value) {
            this._type = value;
            this.notifyPropertyChanged('type');
        }
    }

    public get sendAt(): string {
        return this._sendAt;
    }

    public set sendAt(value: string) {
        if (this._sendAt != value) {
            this._sendAt = value;
            this.notifyPropertyChanged('sendAt');
        }
    }

    readonly items: MailingMessageItem[] = [];
}
