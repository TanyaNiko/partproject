import { Random } from '../../core/services/random.service';
import { MailingMessageItemDto } from '../services/mailing-service/mailing-message-item-dto';
import { ModelBase } from './model-base';

export enum MailingMessageType {
    text = 'text',
    image = 'image',
    audio = 'audio',
    link = 'link'
}

export enum MailingMessageFields {
    text = 'текст',
    link = 'ссылка',
    image = 'изображение',
    audio = 'аудио'
}

export namespace MailingMessageType {
    export function getType(field: MailingMessageFields) {
        switch (field) {
            case MailingMessageFields.text:
                return MailingMessageType.text;
            case MailingMessageFields.image:
                return MailingMessageType.image;
            case MailingMessageFields.audio:
                return MailingMessageType.audio;
            case MailingMessageFields.link:
                return MailingMessageType.link;
        }
    }
}

export class MailingMessageItem extends ModelBase {
    static fromDto(dto: MailingMessageItemDto, mailingId: number, random:Random) {
        if (!dto.id) {
            dto.id = random.guid();
        }
        const obj = new MailingMessageItem(dto.id, mailingId);
        obj._url = dto.url;
        obj._text = dto.text;
        obj._type = dto.type;

        return obj;
    }

    toDto(): MailingMessageItemDto {
        return {
            id: this.id,
            url: this.url,
            text: this.text,
            type: this.type,
        };
    }

    private _type: MailingMessageType;
    private _text?: string;
    private _url?: string;

    private constructor(public readonly id: string, public readonly mailingId: number) {
        super();
    }

    public get type(): MailingMessageType {
        return this._type;
    }
    public set type(value: MailingMessageType) {
        if (this._type != value) {
            this._type = value;
            this.notifyPropertyChanged('type');
        }
    }
    public get text(): string {
        return this._text;
    }
    public set text(value: string) {
        if (this._text != value) {
            this._text = value;
            this.notifyPropertyChanged('text');
        }
    }
    public get url(): string {
        return this._url;
    }
    public set url(value: string) {
        if (this._url != value) {
            this._url = value;
            this.notifyPropertyChanged('url');
        }
    }
}
