import { Component, OnInit } from '../../../a6.shims';
import { ChatbotsApiService } from '../../../core/api/chat-bot.services';
import { BotResponseDto } from '../../../core/api/chat-bot.services/get-bots-response.dto';
import { ITagAutocomplete } from '../../../core/api/tags.service/tag-template.dto';
import { IModalComponentController } from '../../../core/services/modal.service';

@Component({
    selector: 'selection-bots-modal.component',
    controllerAs: 'vm',
    templateUrl: 'app/mailing/components/selection-bots-modal.component/selection-bots-modal.component.html',
})
export class SelectionBotsModalComponent implements OnInit, IModalComponentController<BotResponseDto[]> {

    $close: (result?: BotResponseDto[]) => void;
    $dismiss: () => void;

    selectedBots: ITagAutocomplete[];
    bots: BotResponseDto[] = [];

    constructor(private readonly chatbotsApiService: ChatbotsApiService) {}

    async ngOnInit() {
        try {
            const response = await this.chatbotsApiService.getBots();

            this.bots = response.chat_dialogs;
        }
        catch (e) {
            console.error(e);
        }
    }

    autocomplete(query: string): ITagAutocomplete[] {
        const existing = {};

        return <ITagAutocomplete[]>(this.bots as BotResponseDto[])
            .filter((bot: BotResponseDto): boolean => {
            return <number>bot.name.toLowerCase().indexOf(query.toLowerCase()) !== -1;
        })
            .map((x: BotResponseDto): ITagAutocomplete => {
                let name = x.name;

                if (existing[name]) {
                    name = `${name} (${existing[name]++})`;
                }
                else {
                    existing[name] = 1;
                }
                return <ITagAutocomplete> {
                    id: x.id,
                    text: name
                };
            });
    }

    close() {
        if (this.$dismiss) {
            this.$dismiss();
        }
    }

    save() {
        const selectedBots = (this.selectedBots || []).map(x => {
            return this.bots.find(t => t.id === x.id);
        });

        if (this.$close && this.selectedBots.length) {
            this.$close(selectedBots);
        }
    }
}
