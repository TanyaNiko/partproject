import { Component } from '../../../a6.shims';
import { IModalComponentController } from '../../../core/services/modal.service';

@Component({
    selector: 'get-name-modal.component',
    controllerAs: 'vm',
    templateUrl: 'app/mailing/components/get-name-modal.component/get-name-modal.component.html',
})
export class GetNameModalComponent implements IModalComponentController<string> {
    private _name: string = null;
    private _nameWasChanged = false;

    $close: (result?: string) => void;
    $dismiss: () => void;

    get name(): string { return this._name; }

    set name(value: string) {
        if (this.name != value) {
            this._name = value;
            this._nameWasChanged = true;
        }
    }

    get nameIsValid() { return !this._nameWasChanged || this.hasName; }

    get hasName() { return !!this.name; }

    close() {
        if (this.hasName && this.$close) {
            this.$close(this._name);
        }
    }
}
