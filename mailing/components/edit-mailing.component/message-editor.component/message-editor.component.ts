import { Component, Input } from "../../../../a6.shims";
import { Random } from '../../../../core/services/random.service';
import { MailingMessage } from '../../../models/mailing-message';
import { MessageType } from '../../../services/mailing-service/mailing-message-dto';
import { PartBase } from '../part-base';

@Component({
    selector: 'mailing.messages',
    templateUrl: 'app/mailing/components/edit-mailing.component/message-editor.component/message-editor.component.html',
    controllerAs: 'vm',
})
export class MessageEditor extends PartBase<MailingMessage[]> {

    @Input() id: number;

    constructor(private readonly random: Random) {
        super();
    }

    get messages(): MailingMessage[] {
        return this.data || [];
    }

    private createItem(): MailingMessage {
        return MailingMessage.fromDto({
            id: this.random.guid(),
            items: [],
            name: 'Новое сообщение',
            type: MessageType.Instantly,
            sendAt: null,
        }, this.id, this.random);
    }

    addMessage() {
        this.messages.push(this.createItem());
    }

    onDrop(srcIndex, targetIndex) {
        this.messages.splice(targetIndex, 0, this.messages[srcIndex]);

        if (targetIndex <= srcIndex) {
            srcIndex++;
        }
        this.messages.splice(srcIndex, 1);
        return true;
    }

    removeMessage(messageId: string) {
        const index = this.messages.findIndex(x => x.id == messageId);
        if (index !== -1) {
            this.messages.splice(index, 1);
        }
    }

    get hasMessages() { return this.messages && this.messages.length > 0; }
}
