import { angular, Component } from "../../../../../a6.shims";
import { Random } from '../../../../../core/services/random.service';
import { MailingMessage } from '../../../../models/mailing-message';
import { MailingMessageFields, MailingMessageItem, MailingMessageType } from '../../../../models/mailing-message-item';
import { MessageType } from '../../../../services/mailing-service/mailing-message-dto';
import { PartBase } from '../../part-base';

declare const moment;

@Component({
    selector: 'message-condition',
    templateUrl: 'app/mailing/components/edit-mailing.component/message-editor.component/message-item.component/message-item.component.html',
    controllerAs: 'vm',
})
export class MessageItemComponent extends PartBase<MailingMessage> {

    fields = MailingMessageFields;
    fieldTypes = MailingMessageType;

    get instantSend(): boolean {
        return this.data.type === MessageType.Instantly;
    }

    set instantSend(value: boolean) {
        this.data.type = value ? MessageType.Instantly : MessageType.ByDate;
        if (this.data.type === MessageType.ByDate) {
            this.data.sendAt = moment().format("DD-MM-YYYY HH:mm");
        }
        else{
            this.data.sendAt = null;
        }
    }

    constructor(private readonly random: Random) {
        super();
    }

    get items(): MailingMessageItem[] {
        return this.data.items;
    }

    set items(items: MailingMessageItem[]) {
        if (this.data.items !== items) {
            this.data.items.length = 0;
            this.data.items.push(...items);
        }
    }

    get sendAt(): string {
        return this.data.sendAt;
    }

    set sendAt(date: string) {
        this.data.sendAt = date;
    }

    get hasItems(): boolean {
        return this.data && this.items && this.items.length > 0;
    }

    showItemByType(type: MailingMessageFields, itemType: MailingMessageFields): boolean {
        return type === itemType;
    }

    get sendAtVisible() {
        return this.done || this.editable && !this.instantSend;
    }

    get instantSendVisible() {
        return this.instantSend && !this.editable || this.editable;
    }

    createItem(field: MailingMessageFields) {
        const newItem = MailingMessageItem.fromDto({
            type: MailingMessageType.getType(field),
            text: '',
            url: '',
            id: this.random.guid(),
        }, this.data.mailingId, this.random);
        this.items.push(newItem);
    }

    onDrop(srcIndex, targetIndex) {
        this.items.splice(targetIndex, 0, this.items[srcIndex]);

        if (targetIndex <= srcIndex) {
            srcIndex++;
        }
        this.items.splice(srcIndex, 1);
        return true;
    }

    removeItem(itemGuid) {
        const index = this.items.findIndex(x => x.id == itemGuid);
        if (index !== -1) {
            this.items.splice(index, 1);
        }
    }

    remove() {
        if (angular.isFunction(this.removeRequest)) {
            this.removeRequest();
        }
    }
}
