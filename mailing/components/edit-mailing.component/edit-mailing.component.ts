import * as Enumerable from 'linq';

import { Component, IStateService, OnInit } from '../../../a6.shims';
import { ChatbotsApiService } from '../../../core/api/chat-bot.services';
import { BotResponseDto } from '../../../core/api/chat-bot.services/get-bots-response.dto';
import { Random } from '../../../core/services/random.service';
import { FilterGroup } from '../../models/filter-group';
import { MailingMessage } from '../../models/mailing-message';
import { MailingService } from '../../services/mailing-service';
import { MailingDto } from '../../services/mailing-service/mailing-dto';
import { MailingStatus } from '../../services/mailing-service/mailing-status';

@Component({
    selector: 'mailing.edit',
    controllerAs: 'vm',
    templateUrl: 'app/mailing/components/edit-mailing.component/edit-mailing.component.html',
})
export class EditMailingComponent implements OnInit {

    id: number;
    private _bots: BotResponseDto[];
    status: MailingStatus;

    constructor(private $stateParams: any,
                private readonly $state: IStateService,
                private readonly chatbotsApiService: ChatbotsApiService,
                private readonly svc: MailingService,
                private random: Random) {
        this.id = this.$stateParams.id || 0;
        this.name = this.$stateParams.name;
    }

    name: string;

    get hasChanges(): boolean {
        // TODO add change tracking
        return this.isNew ||
               Enumerable.from(this.filterGroups).selectMany(x => x.filters).any(x => x.hasChanges()) ||
               Enumerable.from(this.messages).any(x => x.hasChanges());
    }

    readonly filterGroups: FilterGroup[] = [];
    readonly messages: MailingMessage[] = [];
    readonly selectedBots: BotResponseDto[] = [];

    readonly headerEditorConfig = {
        menubar: false,
        inline: true,
        plugins: [
            //'powerpaste',
        ],
        toolbar: false,
        powerpaste_word_import: 'clean',
        powerpaste_html_import: 'clean',
    };

    get editable() {
        return this.status === MailingStatus.Inactive;
    }

    get showStart() {
        return this.validMessages && this.selectedBots && this.selectedBots.length;
    }

    get validMessages() {
        if (!this.messages || !this.messages.length) {
            return false;
        }

        let emptyItems = true;
        this.messages.forEach(x => {
            if (x.items && x.items.length) {
                emptyItems = false;
            }
        });

        return !emptyItems;
    }

    get statusColor(): string {
        return MailingStatus.getColor(this.status);
    }

    get statusName() {
        return MailingStatus.get(this.status);
    }

    get isNew(): boolean { return !this.id; }

    get allBots(): BotResponseDto[] { return this._bots; }

    async ngOnInit() {
        const response = await this.chatbotsApiService.getBots();

        this._bots = response.chat_dialogs;

        // init with defaults
        this.filterGroups.length = 0;
        this.messages.length = 0;
        this.selectedBots.length = 0;

        if (this.isNew) {
            this.resetToDefaults();
        }
        else {
            // load data from backend
            const dto = await this.svc.getMailing(this.id);
            this.initializeFromDto(dto);
        }
    }

    async save() {
        const messages = (this.messages || []).map(x => x.toDto());
        const sortMessagesByDate = MailingMessage.sortByDate(messages);
        const result = await this.svc.upsert(
            this.id,
            this.name,
            this.status,
            sortMessagesByDate,
            (this.selectedBots || []).map(x => x.id),
            (this.filterGroups || []).map(x => x.toDto()));

        this.initializeFromDto(result);
        if (this.id != result.id) {
            this.id = result.id;
            this.$state.go('^.edit', { id: result.id, name: result.name });
        }
    }

    private resetToDefaults() {
        this.status = MailingStatus.Inactive;
        this.filterGroups.length = 0;
        this.messages.length = 0;
        this.selectedBots.length = 0;
    }

    async start() {
        this.status = MailingStatus.InProcess;
        try {
            await this.save();
        }
        catch (e) {
            console.error(e);
        }
    }

    private initializeFromDto(dto: MailingDto) {
        this.resetToDefaults();

        this.name = dto.name || `Рассылка ${dto.id}`;
        this.status = dto.status || MailingStatus.Inactive;
        this.filterGroups.push(...(dto.filter_groups || []).map(x => FilterGroup.fromDto(x, this.random)));
        this.messages.push(...(dto.messages || []).map(x => MailingMessage.fromDto(x, this.id, this.random)));
        this.selectedBots.push(...Enumerable.from(dto.bots || []).select(x => {
            return this.allBots.find(t => t.id == x);
        }).where(x => !!x).toArray());
    }
}
