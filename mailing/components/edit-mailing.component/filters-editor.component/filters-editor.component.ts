import { Component, Input } from "../../../../a6.shims";
import { Random } from "../../../../core/services/random.service";
import { FilterGroup } from "../../../models/filter-group";
import { PartBase } from '../part-base';

@Component({
    selector: 'mailing.filters',
    templateUrl: 'app/mailing/components/edit-mailing.component/filters-editor.component/filters-editor.component.html',
    controllerAs: 'vm'
})
export class FiltersEditor extends PartBase<FilterGroup[]>{

    constructor(private readonly random: Random) {
        super();
    }

    private createItem(): FilterGroup {
        return new FilterGroup(this.random.guid());
    }

    addFilterGroup() {
        this.data.push(this.createItem());
    }

    insertFilterGroup(index) {
        this.data.splice(index, 0, this.createItem());
    }

    removeGroup(groupId: string) {
        const index = this.data.findIndex(x => x.id == groupId);
        if (index !== -1) {
            this.data.splice(index, 1);
        }
    }

    get hasGroups(): boolean { return this.data && this.data.length > 0; }
}
