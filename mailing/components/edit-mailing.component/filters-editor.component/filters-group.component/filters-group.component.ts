import { Component, angular, OnInit, IScope } from "../../../../../a6.shims";
import { LeadInfoService } from "../../../../../core/api/lead-info.service";
import { Random } from "../../../../../core/services/random.service";
import { Filter } from "../../../../models/filter";
import { FilterType } from "../../../../services/mailing-service/filter-type";
import { PartBase } from '../../part-base';
import { FilterGroup } from "../../../../models/filter-group";

interface FieldDefinition {
    id: string;
    text: string
}

@Component({
    selector: 'filters-group',
    templateUrl: 'app/mailing/components/edit-mailing.component/filters-editor.component/filters-group.component/filters-group.component.html',
    controllerAs: 'vm'
})
export class FiltersGroupComponent extends PartBase<FilterGroup> implements OnInit {
    private _fields: FieldDefinition[];

    constructor(private readonly service: LeadInfoService,
        private readonly random: Random,
        private readonly $scope: IScope) {
        super();
    }

    get showComponent(){
        return this.editable || this.data.filters && this.data.filters.length;
    }
    async ngOnInit() {
        const fields = await this.service.getCustomFields();
        this._fields = fields.map(x => {
            return {
                id: x.slug,
                text: x.label
            };
        });

        this.$scope.$apply();
    }

    delete() {
        if (angular.isFunction(this.removeRequest)) {
            this.removeRequest();
        }
    }

    get fields() { return this._fields; }

    get conditions(): Filter[] { return this.data.filters; }

    createCondition(field: FieldDefinition) {
        const obj = Filter.fromDto({
            id: this.random.guid(),
            operand: null,
            type: FilterType.property,
            field: field.id,
        }, this.random);

        this.conditions.push(obj);
    }

    createTagCondition() {
        const obj = Filter.fromDto({
            id: this.random.guid(),
            type: FilterType.tag,
            operand: null
        }, this.random);

        this.conditions.push(obj);
    }


    removeCondition(conditionId: string) {
        const index = this.conditions.findIndex(x => x.id == conditionId);
        if (index !== -1) {
            this.conditions.splice(index, 1);
        }
    }
}
