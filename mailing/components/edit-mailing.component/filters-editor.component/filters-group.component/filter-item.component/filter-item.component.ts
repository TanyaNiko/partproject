import { angular, Component, IScope, OnInit } from "../../../../../../a6.shims";
import { LeadInfoService } from "../../../../../../core/api/lead-info.service";
import { CustomFieldDTO } from "../../../../../../core/api/lead-info.service/custom-field.dto";
import { TagsService } from '../../../../../../core/api/tags.service';
import { ITagAutocomplete, ITagTemplate } from '../../../../../../core/api/tags.service/tag-template.dto';
import { Operand } from "../../../../../../core/services/conditions/operands";
import { OperandGroupDictionary, OperandGroupKey } from "../../../../../../core/services/conditions/operands-group";
import { EntityTypes } from '../../../../../../core/services/entity-types';
import { Filter } from "../../../../../models/filter";
import { FilterType } from "../../../../../services/mailing-service/filter-type";
import { PartBase } from '../../../part-base';

@Component({
    selector: 'filter-condition',
    templateUrl: 'app/mailing/components/edit-mailing.component/filters-editor.component/filters-group.component/filter-item.component/filter-item.component.html',
    controllerAs: 'vm',
})
export class FilterItemComponent extends PartBase<Filter> implements OnInit {
    private _fields: CustomFieldDTO[] = [];
    private _allTags: ITagTemplate[] = [];

    public selectedTags: ITagAutocomplete[];

    constructor(private readonly leadInfo: LeadInfoService,
                private readonly $scope: IScope,
                private readonly tagsService: TagsService) {
        super();
    }

    async ngOnInit(): Promise<void> {
        try {
            if (this.tagIdentification) {
                this._allTags = await this.tagsService.fetchAll() || [];

                this.selectedTags = this.data.tags;
            }

            this._fields = await this.leadInfo.getCustomFields() || [];
        }
        catch (e) {
            console.error(e);
        }
        finally {
            this.$scope.$apply();
        }
    }

    public tagsAutocomplete(query: string): ITagAutocomplete[] {
        return <ITagAutocomplete[]>(this._allTags as ITagTemplate[] || [])
            .filter((tag: ITagTemplate): boolean => {
                return <number>tag.name.toString().toLowerCase().indexOf(query.toString().toLowerCase()) !== -1;
            })
            .map((tag: ITagTemplate): ITagAutocomplete => {
                return <ITagAutocomplete> {
                    text: tag.name,
                };
            });
    }

    public async attachTag(text: string): Promise<void> {
        const index = this._allTags.findIndex(tag => tag.name === text);

        if (index === -1) {
            await this.tagsService.attach(text, EntityTypes.MAILING_MESSAGE)
        }

        this.data.tags = text
    }

    public detachTag(text: string) {
        this.data.deleteTag(text)
    }

    private getField(): CustomFieldDTO {
        if (this.data &&
            this.data.type == FilterType.property &&
            this.data.field) {
            return this._fields.find(x => x.slug == this.data.field);
        }

        return null;
    }

    private get inputType(): string {
        const field = this.getField();
        if (field && field.type) {
            if (field.type == 'integer') {
                return 'number';
            }
        }

        return 'text';
    }

    public get tagIdentification(): boolean {
        return this.data.type === FilterType.tag;
    }

    get fieldName(): string {
        if (this.tagIdentification) {
            return 'Тэг';
        }
        const field = this._fields.find(x => x.slug === this.data.field);

        return field ? field.label : '';
    }

    get conditions(): Operand[] {
        if (!this.data) {
            return [];
        }

        if (this.data.type === FilterType.tag) {
            return OperandGroupDictionary.get(OperandGroupKey.contains).operands;
        }

        const field = this.getField();
        return OperandGroupDictionary.getByField(field).operands;
    }

    get condition(): string { return this.data ? this.data.operand : null; }

    set condition(value: string) {
        if (this.data) {
            this.data.operand = value;
        }
    }

    public remove(): void {
        if (angular.isFunction(this.removeRequest)) {
            this.removeRequest();
        }
    }
}
