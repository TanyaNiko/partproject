import { angular, Input, Output } from "../../../a6.shims";
import { MailingStatus } from '../../services/mailing-service/mailing-status';

export abstract class PartBase<T> {
    @Input() data: T;
    @Output() removeRequest;
    @Input() status: MailingStatus;

    get editable() {
        return this.status === MailingStatus.Inactive;
    }

    get done() {
        return this.status === MailingStatus.Done;
    }

    remove() {
        if (angular.isFunction(this.removeRequest)) {
            this.removeRequest();
        }
    }
}
