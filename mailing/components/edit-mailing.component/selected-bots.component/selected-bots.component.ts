import { Component } from '../../../../a6.shims';
import { BotResponseDto } from '../../../../core/api/chat-bot.services/get-bots-response.dto';
import { ModalService } from '../../../../core/services/modal.service';
import { SelectionBotsModalComponent } from '../../selection-bots-modal.component/selection-bots-modal.component';
import { PartBase } from '../part-base';

@Component({
    selector: 'mailing.selected-bots',
    controllerAs: 'vm',
    templateUrl: 'app/mailing/components/edit-mailing.component/selected-bots.component/selected-bots.component.html',
})
export class SelectedBotsComponent extends PartBase<BotResponseDto[]> {
    get bots(): BotResponseDto[] { return this.data || []; }

    get hasBots(): boolean { return this.bots && this.bots.length > 0; }

    constructor(private readonly modal: ModalService) {
        super();
    }

    removeBot(botId: number) {
        const index = this.bots.findIndex(x => x.id == botId);
        if (index !== -1) {
            this.bots.splice(index, 1);
        }
    }

    async addBot() {
        const newItems: BotResponseDto[] = await this.modal.showComponentAsModal<BotResponseDto[]>(SelectionBotsModalComponent);

        if (newItems && newItems.length) {
            this.bots.push(...newItems.filter(x => this.bots.findIndex(t => t.id === x.id) === -1));
        }
    }
}
