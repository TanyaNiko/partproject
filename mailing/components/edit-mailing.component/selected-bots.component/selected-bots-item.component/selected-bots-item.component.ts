import { Component } from '../../../../../a6.shims';
import { BotUtilities } from '../../../../../core/api/chat-bot.services/bots-utilities';
import { BotResponseDto } from '../../../../../core/api/chat-bot.services/get-bots-response.dto';
import { PartBase } from '../../part-base';

@Component({
    selector: 'mailing.selected-bot-item',
    controllerAs: 'vm',
    templateUrl: 'app/mailing/components/edit-mailing.component/selected-bots.component/selected-bots-item.component/selected-bots-item.component.html',
})
export class SelectedBotsItemComponent extends PartBase<BotResponseDto> {
    get name(): string {
        return this.data.name;
    }

    get emptyBots(): boolean {
        return !this.messengers.length || this.messengers.findIndex(x => this.showBot(x)) === -1;
    }

    get messengers() {
        return Object.getOwnPropertyNames(this.data.settings || {});
    }

    getPlaceholder(messenger) {
        return BotUtilities.getPlaceholder(messenger);
    }

    showBot(messenger): boolean {
        return BotUtilities.showBot(messenger);
    }

    workBot(messenger): boolean {
        return BotUtilities.workBot(messenger);
    }
}
