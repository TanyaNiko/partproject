import { Component, OnInit, IStateService } from '../../a6.shims';
import { UserService } from "../../core/api/user.service";

@Component({
    selector: 'app.mailing',
    template: `<navbar></navbar>
    <sidebar></sidebar>
    
    <div class="app-content">
      <ui-view></ui-view>
    </div>`,
    controllerAs: 'vm',
})
export class MailingComponent implements OnInit {
    constructor(
        private readonly $state: IStateService,
        private readonly userService: UserService) {
    }

    async ngOnInit() {
        // TODO remove this check after mailing module is implemented completely
        const user = await this.userService.getUserInfo();
        if (!user.can_admin) {
            this.$state.go('funnels.funnelsPage');
        }
    }
}
