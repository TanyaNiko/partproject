import { Component, Input, Output } from '../../../../a6.shims';
import { MailingInfoItem } from '../../../models/mailing-info-item';
import { MailingService } from '../../../services/mailing-service';
import { MailingStatus } from '../../../services/mailing-service/mailing-status';

@Component({
    selector: 'mailing-item',
    controllerAs: 'vm',
    templateUrl: 'app/mailing/components/mailing-page.component/mailing-item.component/mailing-item.component.html',
})
export class MailingItemComponent {

    showBundleOfMailings: boolean;

    private _messages: MailingInfoItem[] = null;

    get statusColor(): string {
        return MailingStatus.getColor(this.data.status);
    }

    @Input() data: MailingInfoItem;
    @Output() onDeleted: () => void;
    @Output() onDuplicate: () => void;

    get messages() { return this._messages; }

    constructor(private readonly svc: MailingService) {}

    onItemDeleted() {
        if (this.onDeleted) {
            this.onDeleted();
        }
    }

    onItemDuplicated() {
        if (this.onDuplicate) {
            this.onDuplicate()
        }
    }

    async onOpenBundleOfMailings() {
        this.showBundleOfMailings = !this.showBundleOfMailings;

        if (!this._messages) {
            const response = await this.svc.getMailingInfo(this.data.id);
            this._messages = response.map(x => MailingInfoItem.fromDto(x));
        }
    }
}
