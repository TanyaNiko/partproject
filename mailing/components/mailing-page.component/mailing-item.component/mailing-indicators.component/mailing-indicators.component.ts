import { Component, Output, Input, IStateService } from '../../../../../a6.shims';
import { MailingInfoItem } from '../../../../models/mailing-info-item';
import { MailingMessengerStatistics } from '../../../../services/mailing-service/mailing-messenger-statistics';
import { MailingService } from '../../../../services/mailing-service';
import { MessageBoxService } from '../../../../../core/services/uiMessageBox.service';

@Component({
    selector: 'mailing-indicators',
    controllerAs: 'vm',
    templateUrl: 'app/mailing/components/mailing-page.component/mailing-item.component/mailing-indicators.component/mailing-indicators.component.html',
})
export class MailingIndicatorsComponent {

    constructor(private readonly $state: IStateService,
        private readonly svc: MailingService,
        private readonly msgbox: MessageBoxService) {

    }

    stateChain: boolean = false;

    @Output() onOpen: () => void = null;
    @Output() onDeleted: () => void = null;
    @Output() onDuplicate: () => void = null;

    @Input() data: MailingInfoItem;
    @Input() isInnerItem: boolean = false;

    private get dataInternal(): MailingInfoItem {
        return this.data || MailingInfoItem.empty;
    }

    get messengers(): MailingMessengerStatistics[] { return this.dataInternal.messengers; }

    get isEmptyMessengers(): boolean { return !!this.dataInternal.messengers.length; }

    get dateSend(): string { return this.dataInternal.startAt ? this.dataInternal.startAt : this.dataInternal.sendAt; }

    get name(): string { return this.dataInternal.name; }

    get sent(): number { return this.dataInternal.overallStatistics.sent; }

    get delivered(): number { return this.dataInternal.overallStatistics.delivered; }

    get clicked(): number { return this.dataInternal.overallStatistics.clicked; }

    edit() {
        if (this.data && this.isInnerItem) {
            this.$state.go('mailing.edit', { id: this.data.id, name: this.data.name });
        }
    }

    async delete() {
        if (this.data &&
            await this.msgbox.yesNo('Вы действительно хотите удалить эту рассылку?')) {
                await this.svc.delete(this.data.id);

                if (this.onDeleted) {
                    this.onDeleted();
                }
        }
    }

    duplicate() {
        if (this.onDuplicate) {
            this.onDuplicate()
        }
    }

    openMinMailing() {
        if (this.onOpen) {
            this.stateChain = !this.stateChain;
            this.onOpen();
        }
    }
}
