import { Component, Input } from '../../../../../../a6.shims';
import { MessengerSlugs } from '../../../../../../core/services/messenger-slugs';
import { MailingMessengerStatistics } from '../../../../../services/mailing-service/mailing-messenger-statistics';

@Component({
    selector: 'mailing-social',
    controllerAs: 'vm',
    templateUrl: 'app/mailing/components/mailing-page.component/mailing-item.component/mailing-indicators.component/mailing-social.component/mailing-social.component.html',
})
export class MailingSocialComponent {
    @Input() private data: MailingMessengerStatistics;

    get messenger(): MessengerSlugs { return this.data ? this.data.messenger : null; }

    get checkedStatisticsData(): boolean { return this.data && (this.data.sent >= 0 && this.data.delivered >= 0 && this.data.clicked >= 0); }

    get statistics(): string {
        return this.checkedStatisticsData ? `${this.data.sent}/${this.data.delivered}/${this.data.clicked}` : 'Нет данных';
    }
}
