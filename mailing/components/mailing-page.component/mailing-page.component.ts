import { Component, IStateService, OnInit } from '../../../a6.shims';
import { ModalService } from '../../../core/services/modal.service';
import { MailingInfoItem } from '../../models/mailing-info-item';
import { MailingService } from '../../services/mailing-service';
import { GetNameModalComponent } from '../get-name-modal.component/get-name-modal.component';

@Component({
    selector: 'mailing.view',
    controllerAs: 'vm',
    templateUrl: 'app/mailing/components/mailing-page.component/mailing-page.component.html',
})
export class MailingPageComponent implements OnInit {
    private _mailings: MailingInfoItem[];

    get mailings(): MailingInfoItem[] { return this._mailings; }

    constructor(
        private readonly $state: IStateService,
        private readonly svc: MailingService,
        private readonly modal: ModalService) {
    }

    async ngOnInit() {
        const response = await this.svc.getMailings();
        this._mailings = (response.items || []).map(x => MailingInfoItem.fromDto(x));
    }

    async duplicate(selectedId: number) {
        try {
            const item = await this.svc.copy(selectedId);
            if (item) {
                this.mailings.unshift(MailingInfoItem.fromDto(item));
            }
        }
        catch (e) {
            console.error(e);
        }
    }

    removeMailingFromList(mailing: MailingInfoItem) {
        const idx = this.mailings.indexOf(mailing);
        if (idx != -1) {
            this.mailings.splice(idx, 1);
        }
    }

    async createMailing() {
        const name: string = await this.modal.showComponentAsModal<string>(GetNameModalComponent);

        if (name) {
            this.$state.go(`mailing.create`, { name: name });
        }
    }
}
