import { MailingMessageType } from '../../models/mailing-message-item';

export interface MailingMessageItemDto {
    id: string;
    type: MailingMessageType;
    text?: string;
    url?: string;
}
