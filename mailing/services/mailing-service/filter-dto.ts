import { FilterType } from "./filter-type";
export interface FilterDto {
    id: string;
    type: FilterType;
    value?: string;
    tags?: string[];
    field?: string;
    operand: string;
}
