import { FilterDto } from "./filter-dto";

export interface FilterGroupDto {
    id: string;
    filters: FilterDto[];
}
