import { MailingMessageItemDto } from './mailing-message-item-dto';

export interface MailingMessageDto {
    id: string;
    name: string;
    sendAt: string; // null if send immediately -otherwise- date/time string
    items: MailingMessageItemDto[];
    type: MessageType;
}

export enum MessageType {
    Instantly = 1,
    ByDate = 2
}
