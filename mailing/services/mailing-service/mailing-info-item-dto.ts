import { MailingStatus } from "./mailing-status";
import { MailingMessengerStatistics } from "./mailing-messenger-statistics";
export interface MailingInfoItemDto {
    id: number;
    name: string;
    startAt?: string;
    sendAt?: string;
    status?: MailingStatus;
    messengers: MailingMessengerStatistics[];
    type?: number;
}
