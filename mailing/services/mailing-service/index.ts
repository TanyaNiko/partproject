import { Injectable } from "../../../a6.shims";
import { ApiServiceBase, ResponseStatus } from "../../../core/api-service-base";
import { HttpService } from "../../../core/services/http.service";
import { FilterGroupDto } from './filter-group-dto';
import { MailingDto } from "./mailing-dto";
import { MailingInfoDto } from "./mailing-info-dto";
import { MailingInfoItemDto } from "./mailing-info-item-dto";
import { MailingMessageDto } from './mailing-message-dto';
import { MailingStatus } from './mailing-status';

@Injectable()
export class MailingService extends ApiServiceBase {
    constructor(http: HttpService) {
        super(http, 'api/v1/admin/chat/mailings/');
    }

    /**
     * get list of mailings
     * @returns MailingInfoDto
     */
    async getMailings(): Promise<MailingInfoDto> {
        const response = await this._get<{ result: MailingInfoDto, status: ResponseStatus }>('/');
        if (response.status !== ResponseStatus.success) {
            throw response;
        }
        return response.result;
    }

    /**
     * delete mailing by id
     * @param mailingId     number  identifier of mailing to delete
     */
    async delete(mailingId: number): Promise<void> {
        await this._delete(`/${mailingId}`);
    }

    async copy(id: number) {
        const response = await this._post<{ result: MailingInfoItemDto, status: ResponseStatus }>(`/copy/${id}`, {});

        if (response.status !== ResponseStatus.success) {
            throw response;
        }

        const result = response.result;

        if (!result) {
            throw new Error('copy has empty response');
        }

        return result;
    }

    /**
     * get mailing messages by id
     * @param id    number  identifier of mailing to get messages for
     * @returns MailingInfoItemDto[]
     */
    async getMailingInfo(id: number): Promise<MailingInfoItemDto[]> {
        const response = await this._get<{ result: MailingInfoItemDto[], status: ResponseStatus }>(`/messages/${id}`);
        if (response.status !== ResponseStatus.success) {
            throw response;
        }

        return response.result || [];
    }

    /**
     * get one mailing by id GET http://dev.pipercat.ru/api/v1/admin/chat/mailings/{id}
     * @param id    number   identifier of mailing
     */
    async getMailing(id: number): Promise<MailingDto> {
        const response = await this._get<{ result: MailingDto, status: ResponseStatus }>(`/${id}`);
        if (response.status !== ResponseStatus.success) {
            throw response;
        }

        return response.result;
    }

    /**
     * Create or update mailing
     * @param dto MailingDto Mailing to save. If id == 0 - create new entry, otherwise - update existing one
     */
    async upsert(id: number,
                 name: string,
                 status: MailingStatus,
                 messages: MailingMessageDto[],
                 bots: number[],
                 filterGroups: FilterGroupDto[]): Promise<MailingDto> {
        const data: MailingDto = {
            id: Number(id || 0),
            messages: messages || null,
            bots: bots || null,
            status: status,
            filter_groups: filterGroups || null,
            name: name,
        };

        const response = await this._post<{ result: MailingDto, status: string }>(`/${id}`, data);
        const result = response.result;

        if (!result) {
            throw new Error('upsert has empty response');
        }

        return result;
    }
}
