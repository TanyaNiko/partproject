import { MailingInfoItemDto } from "./mailing-info-item-dto";
// ------------------------------------------------------------------------------------------------------------
// Main page
// ------------------------------------------------------------------------------------------------------------
/**
 * get all mailings
 * api call like GET /api/mailing[?page=2&size=10]
 * response: MailingInfoDto[]
 *
 * get mailing messages for mailing by ID
 * api call like GET /api/mailing/messages/{id}
 * response: MailingInfoItemDto[]
 */
export interface MailingInfoDto {
    items: MailingInfoItemDto[];
    total: number;
    page: number;
    pageSize: number;
}
