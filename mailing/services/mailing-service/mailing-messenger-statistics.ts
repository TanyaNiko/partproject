import { MessengerSlugs } from "../../../core/services/messenger-slugs";

export interface MailingMessengerStatistics extends MailingStatistics {
    messenger: MessengerSlugs;
}

export interface MailingStatistics {
    sent: number;
    delivered: number;
    clicked: number;
}
