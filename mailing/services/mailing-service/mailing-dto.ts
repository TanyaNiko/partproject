import { MailingMessageDto } from "./mailing-message-dto";
import { FilterGroupDto } from "./filter-group-dto";
import { MailingStatus } from './mailing-status';

// ------------------------------------------------------------------------------------------------------------
// Create/edit page
// ------------------------------------------------------------------------------------------------------------
/**
 * Get mailing by ID
 *
 * api call like GET /api/mailing/{id}
 * response: MailingDto
 *
 * Create mailing
 * api call like POST /api/mailing -or- /api/mailing/new
 * response: MailingDto with brand new ID
 *
 * Update mailing by ID
 * api call like POST /api/mailing/{id}
 * response: MailingDto
 */
export interface MailingDto {
    id: number;
    name: string;
    status?: MailingStatus;

    messages?: MailingMessageDto[];
    bots?: number[];
    filter_groups?: FilterGroupDto[];
}


