export enum MailingStatus {
    Inactive = 0,
    InProcess = 1,
    Done = 2
}

export namespace MailingStatus {
    export function get(status: MailingStatus): string {
        switch (status) {
            case MailingStatus.Inactive:
                return "не запущена";
            case MailingStatus.InProcess:
                return "в процессе";
            case MailingStatus.Done:
                return "выполнена";
        }
        return '';
    }

    export function getColor(status: MailingStatus): string {
        switch (status) {
            case MailingStatus.Inactive:
                return '#e74c3c';
            case MailingStatus.InProcess:
                return '#eaba37';
            case MailingStatus.Done:
                return '#2ecc71';
            default:
                return 'transparent';
        }
    }
}
