import { NgModule } from '../a6.shims';
import { SelectedBotsItemComponent } from './components/edit-mailing.component/selected-bots.component/selected-bots-item.component/selected-bots-item.component';
import { SelectedBotsComponent } from './components/edit-mailing.component/selected-bots.component/selected-bots.component';
import { GetNameModalComponent } from './components/get-name-modal.component/get-name-modal.component';
import { MessageAudioComponent } from './components/edit-mailing.component/message-editor.component/message-item.component/message-audio.component/message-audio.component';
import { MessageImageComponent } from './components/edit-mailing.component/message-editor.component/message-item.component/message-image.component/message-image.component';
import { MessageItemComponent } from './components/edit-mailing.component/message-editor.component/message-item.component/message-item.component';
import { MessageLinkComponent } from './components/edit-mailing.component/message-editor.component/message-item.component/message-link.component/message-link.component';
import { MessageTextComponent } from './components/edit-mailing.component/message-editor.component/message-item.component/message-text.component/message-text.component';
import { MailingIndicatorsComponent } from './components/mailing-page.component/mailing-item.component/mailing-indicators.component/mailing-indicators.component';
import { MailingSocialComponent } from './components/mailing-page.component/mailing-item.component/mailing-indicators.component/mailing-social.component/mailing-social.component';
import { MailingItemComponent } from './components/mailing-page.component/mailing-item.component/mailing-item.component';
import { SelectionBotsModalComponent } from './components/selection-bots-modal.component/selection-bots-modal.component';

import { MailingService } from './services/mailing-service';

import { MailingComponent } from "./components/mailing.component";
import { MailingPageComponent } from './components/mailing-page.component/mailing-page.component';
import { EditMailingComponent } from './components/edit-mailing.component/edit-mailing.component';
import { MessageEditor } from './components/edit-mailing.component/message-editor.component/message-editor.component';
import { FiltersEditor } from './components/edit-mailing.component/filters-editor.component/filters-editor.component';
import { FilterItemComponent } from './components/edit-mailing.component/filters-editor.component/filters-group.component/filter-item.component/filter-item.component';
import { FiltersGroupComponent } from './components/edit-mailing.component/filters-editor.component/filters-group.component/filters-group.component';


@NgModule({
    declarations: [
        MailingComponent,

        MailingPageComponent,
        MailingItemComponent,
        MailingIndicatorsComponent,
        MailingSocialComponent,
        GetNameModalComponent,

        EditMailingComponent,
        FiltersEditor,
        FiltersGroupComponent,
        FilterItemComponent,
        SelectedBotsComponent,
        SelectedBotsItemComponent,
        SelectionBotsModalComponent,

        MessageEditor,
        MessageItemComponent,
        MessageTextComponent,
        MessageLinkComponent,
        MessageAudioComponent,
        MessageImageComponent
    ],
    providers: [
        MailingService
    ]
})
export class MailingModule {

}
